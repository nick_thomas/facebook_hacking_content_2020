use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;
use std::env;
use std::io::Write;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("In the file {}", filename);
    let contents = fs::read_to_string(filename).unwrap();
    let contents = contents.trim().split("\n").collect::<Vec<&str>>();
    let check_if_vowel = |character: &char| -> bool{
        match character  {
            'A' | 'E' | 'I' | 'O' | 'U' => true,
            _=> false
        }
    };
    let mut output:Vec<u32> = Vec::new();
    for word in contents.iter().skip(1){
        let mut check_unique = HashSet::new();
        word.chars().for_each(|characer: char| {
            check_unique.insert(characer);
            ()
        });
        let total_length = word.len();
        if total_length == 1 || check_unique.len() == 1 {
            output.push(0);
            continue;
        }

        let mut unique_vowel : HashMap<char,usize> = HashMap::new();
        let mut unique_consannts : HashMap<char,usize> = HashMap::new();
        let vowels= word.chars().filter(check_if_vowel).collect::<Vec<char>>();
        let constants = word.chars().filter(|item: &char| {
            let value = check_if_vowel(item);
            !value
        }).collect::<Vec<char>>();
        vowels.iter().for_each(|item: &char| {
            *unique_vowel.entry(*item).or_insert(0)+=1;
            ()
        });
        constants.iter().for_each(|character:&char| {
            *unique_consannts.entry(*character).or_insert(0)+=1;
            ()
        });
        let max_count_vowel = unique_vowel.values().max();
        let max_count_consanants = unique_consannts.values().max();
        let vowel_count:usize= unique_vowel.values().sum();
        let consanant_count :usize = unique_consannts.values().sum();
        if max_count_vowel > max_count_consanants {
            if unique_vowel.len() == 1 {
                output.push(consanant_count as u32);
                continue;
            }
            let consanant_count = (consanant_count - max_count_consanants.unwrap_or(&0)) * 2;
            let seconds =consanant_count + vowel_count;
            output.push(seconds as u32);
        } else {
            if unique_consannts.len() == 1 {
                output.push(vowel_count as u32);
                continue;
            }
            let vowel_count = (vowel_count - max_count_vowel.unwrap_or(&0)) *2;
            let seconds =consanant_count + vowel_count;
            output.push(seconds as u32);
        }
    }
    let mut file = fs::OpenOptions::new()
        .write(true)
        .append(true)
        .create(true)
        .open("./output.txt")
        .unwrap();
    for (index,seconds) in output.iter().enumerate(){
        let output_string = format!("Case #{}: {}\n",index+1,seconds);
        file.write_all(output_string.as_bytes());
    }
}
